These scripts have been used with the following python packages
```
gto==0.1.14
dvc==2.14.0
```
and git 2.30.2.

How to use:
```shell
. <(curl -s https://gitlab.com/francesco-calcavecchia/git-repo-as-versioned-storage/-/raw/main/main.sh)
```
