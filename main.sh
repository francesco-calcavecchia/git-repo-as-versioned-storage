function gto-remote-which {
	# Args:
	# 1. repo url
	# 2. gto which NAME
	# 3. gto which STAGE
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
 	git clone -n -q $1 ${TMP_REPO_FOLDER}
	cd ${TMP_REPO_FOLDER}
	WHICH="$(gto which $2 $3)"
	cd ..
	rm -rf ${TMP_REPO_FOLDER}
	echo "${WHICH}"
}


function dvc-remote-get {
	# Args:
	# 1. dvc get URL (repo url)
	# 2. dvc get --rev (git tag)
	# 3. dvc get PATH (file path)
	# 4. dvc remote modify OPTION
	# 5. dvc remote modify VALUE
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
	git clone -q --depth 1 --branch $2 $1 ${TMP_REPO_FOLDER}
	cd ${TMP_REPO_FOLDER}
	DEFAULT_REMOTE="$(cat .dvc/config | grep -e "remote =" |  sed 's/.*remote = \(.*\)/\1/g')"
	dvc remote modify ${DEFAULT_REMOTE} $4 $5
	dvc pull -R $3
	mv $3 ../$(basename $3)
	cd ..
	rm -rf $TMP_REPO_FOLDER
}


function git-dvc-remote-commit-and-push {
	# Args:
	# 1. repo url
	# 2. absolute path to files to register. IMPORTANT: if it is a folder the path must end with a "/"
	# 3. destination relative path in artifact registry git repo
	# 4. commit message
	# 5. dvc remote modify OPTION
	# 6. dvc remote modify VALUE
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
	echo " === TMP_REPO_FOLDER = ${TMP_REPO_FOLDER}"
	echo " >>> git clone -q --depth 1 $1 ${TMP_REPO_FOLDER}"
	git clone -q --depth 1 $1 ${TMP_REPO_FOLDER} || return 1
	echo " >>> cd ${TMP_REPO_FOLDER}"
	cd "${TMP_REPO_FOLDER}" || return 1
	echo " >>> mkdir -p $3"
	mkdir -p $3 || return 1
	echo " >>> cp -r $2 $3"
	cp -r $2 $3 || return 1
	echo " >>> dvc add -R $3"
	dvc add -R $3 || return 1
	DEFAULT_REMOTE="$(cat .dvc/config | grep -e "remote =" |  sed 's/.*remote = \(.*\)/\1/g')" || return 1
	echo " === DEFAULT_REMOTE = ${DEFAULT_REMOTE}"
	echo " >>> dvc remote modify ${DEFAULT_REMOTE} $5 [MASKED]"
	dvc remote modify ${DEFAULT_REMOTE} $5 $6 || return 1
	echo " >>> dvc push -R $3"
	dvc push -R $3 || return 1
	echo " >>> git pull --tags"
	git pull --tags || return 1
	echo " >>> git commit -m \"$4\""
	git commit -m "$4" || return 1
	echo " >>> git push --repo $1"
	git push --repo $1 || return 1
	echo " >>> cd .."
	cd .. || return 1
	echo " >>> rm -rf $TMP_REPO_FOLDER"
	rm -rf $TMP_REPO_FOLDER || return 1
}



function gto-remote-latest {
	# Args:
	# 1. repo url
	# 2. gto latest NAME
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
 	git clone -n -q $1 ${TMP_REPO_FOLDER}
	cd ${TMP_REPO_FOLDER}
	LATEST="$(gto latest $2)"
	cd ..
	rm -rf ${TMP_REPO_FOLDER}
	echo "${LATEST}"
}


function gto-remote-promote {
	# Args:
	# 1. repo url
	# 2. gto promote NAME
	# 3. gto promote STAGE
	# 4. artifact version (e.g. v0.0.1)
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
 	git clone -q --depth 1 --branch "$2@$4" $1 ${TMP_REPO_FOLDER}
	cd ${TMP_REPO_FOLDER}
	gto promote $2 $3
	git push --tags --repo $1
	cd ..
	rm -rf ${TMP_REPO_FOLDER}
}


function gto-remote-register {
	# Args:
	# 1. repo url
	# 2. gto register NAME (artifact name)
	TMP_REPO_FOLDER=".tmp-model-registry-$(date +'%s')"
	git clone -n -q $1 ${TMP_REPO_FOLDER}
	cd ${TMP_REPO_FOLDER}
	gto register $2
	git push --tags --repo $1
	cd ..
	rm -rf $TMP_REPO_FOLDER
}
